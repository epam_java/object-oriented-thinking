# Object-Oriented-Thinking


### Task Description
Discuss with a mentor which of the following options of a system should be chosen to design the class hierarchy.
##### Table or Card Games simulations:
    Chess
    Checkers
    Narde
    Go
    ...
#### Publicly available devices/services:
    a public printer/copier at a university
    a public coffee/vending machine at a
    university/dormitory/hospital/street
    a public phone booth on a street
    ...
#### Scheduling/Management systems:
    Dormitory accommodation management
    Zoo animals' nutrition management
    Robotized warehouse management
    ...
Please read carefully and do the following:

Design class diagrams picturing classes, their attributes, and relations in the system.
Convert diagrams into Java code.
How this task will be evaluated:
Please be aware that we expect the completed task to meet the following criteria:

Class diagrams picturing classes, their attributes, and relations in the system should be designed.
Diagrams should be converted into Java code.
The task should be submitted as txt, doc, rtf, docx, or pdf document in the form below.