package org.epam;

public interface PrinterCopier {
    void turnOn();

    void turnOff();

    void printDocument();

    void copyDocument();

    void cancelPrinting();

    void cancelCopying();

    void fixPaperJam();

    void connectUser();

    void disconnectUser();

    boolean isPaperAvailable();

    boolean isInkAvailable();
}
