package org.epam;

import java.util.List;

public class PublicPrinterCopier implements PrinterCopier{

    private int paperCount;
    private int InkCapacity;
    private boolean isPrinting;
    private boolean isCopying;
    private boolean isPaperJam;
    private boolean isOn;
    private List<User> connectedUsers;


    private boolean isUserConnected(User user) {
        return connectedUsers.contains(user);
    }

    public void setConnectedUsers(List<User> connectedUsers) {
        this.connectedUsers = connectedUsers;
    }

    public void turnOn() {

    }

    public void turnOff() {

    }

    public void printDocument() {

    }

    public void copyDocument() {

    }

    public void cancelPrinting() {

    }

    public void cancelCopying() {

    }

    public void fixPaperJam() {

    }

    public void connectUser() {

    }

    public void disconnectUser() {

    }

    public boolean isPaperAvailable() {
        return false;
    }

    public boolean isInkAvailable() {
        return false;
    }
}