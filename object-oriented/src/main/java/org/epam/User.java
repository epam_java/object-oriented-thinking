package org.epam;

import sun.security.util.Password;

public class User implements Users{
    String username = null;
    Password password = null;
    int canPrint = 100;
    int canCopy = 100;
    int printedDocuments = 0;

    /***
     * checks username and password to connect
     */
    public void connectUser() {
        /*
        check username and password to connect
         */
        System.out.println("User connected!");

    }
    /***
     * disconnects user
     */
    public void disconnectUser() {
        System.out.println("User disconnected!");
    }
}